import logging
import sys

from scipy.io import netcdf as ncdf
import numpy

class Submesh:

  r'''
  Class to hold information associated with a submesh.
  '''

  def __init__(self):
    
    r'''
    Default constructor, create a default submesh with no information.
    '''

    # Name of the submesh
    self._name = ''

    # Index of the submesh
    self._idx = -1

    # Connectivity information (element node indices)
    self._connect = None

    # Number of elements
    self._nelems = -1

    # Elements
    self._element_numbers = None

  # end __init__



  def __str__(self):

    r'''
    Return a string representation of this object.
    '''

    return '("%s", %i, %i)' % (self._name, self._idx, self._nelems)

  # end __str__



  def name(self):

    r'''
    Retrieve the name of the submesh.
    '''

    return self._name

  # end name()



  def index(self):

    r'''
    Retrieve the index of the submesh.
    '''

    return self._idx

  # end index()



  def connect(self):

    r'''
    Retrieve the connectivity information of submesh.
    '''

    return self._connect

  # end connect()



  def nelems(self):

    r'''
    Retrieve the number of elements in the submesh.
    '''

    return self._nelems

  # end nelems()



  def element_numbers(self):

    r'''
    Retrieve the element numbers for each element in the submesh.
    '''

    return self._element_numbers

  # end element_numbers()



  def set_name(self, name):

    r'''
    Set the name for this submesh.
    '''

    self._name = name

  # end set_name()



  def set_index(self, index):

    r'''
    Set the index for this submesh.
    '''

    self._idx = index

  # end set_index()



  def set_connect(self, connect):

    r'''
    Set the connectivity data for this submesh.
    '''

    self._connect = connect

  # end set_connect()



  def set_nelems(self, nelems):

    r'''
    Set the number of elements for this submesh.
    '''

    self._nelems = nelems

  # end set_nelems()



  def set_element_numbers(self, element_numbers):

    r'''
    Set the element numbers/indices for this submesh.
    '''

    self._element_numbers = element_numbers

  # end set_element_numbers

# end class



class SubmeshManager:

  r'''
  Holds a list of elements and vertices ordered by submesh.
  '''

  def __init__(self, ncfile):

    r'''
    Default constructor, does nothing.
    '''

    # Logging
    stream_handler = logging.StreamHandler(sys.stdout)
    self._log = logging.getLogger(self.__class__.__name__)
    self._log.addHandler(stream_handler)
    self._log.setLevel(logging.DEBUG)

    # NetCDF file object
    self._ncfile = ncfile

    # Submeshes
    self._submeshes = {}

    # Submesh names (kept in the correct order).
    self._submesh_names = []

    # An index that is used to reference the NetCDF variables:
    #    * num_el_in_blk - Number of elements in a block (below).
    #    * connect       - Connectivity information (below).
    i = 0

    # Element block start index.
    elem_map_start = 0

    # Element block end index.
    elem_map_end = 0
    
    # Element map. This is a list of indices which are grouped in to blocks.
    # Each element name corresponds to a block starting at elem_map_start to 
    # elem_map_end (inclusive).
    elem_map = ncfile.variables['elem_map']

    # Submesh names.
    matnames = ncfile.variables['eb_names']

    # Submesh ids.
    matids = ncfile.variables['eb_prop1']

    # Loop round each of the submesh names.
    for matname in matnames:

      # Extract submesh name as a string.
      str_list = [b.decode('ascii') for b in matname]
      str_matname = ''.join(str_list)

      # Add the string version of the submesh name to this objects list of
      # submesh names.
      self._submesh_names.append(str_matname)

      # Extract the number of elements in block i (correspoinding to the 
      # submesh name).
      num_el_in_blk = ncfile.dimensions['num_el_in_blk%i' % (i+1)]
      
      # Create a submesh.
      mat = Submesh()

      # Set name, index, connectivity (node indices for actual elements) and 
      # number of elements for the submesh.
      mat.set_name(str_matname)
      mat.set_index(matids[i])
      mat.set_connect(ncfile.variables['connect%i' % (i+1)])
      mat.set_nelems(num_el_in_blk)

      elem_map_end += num_el_in_blk
      mat.set_element_numbers(elem_map[elem_map_start:elem_map_end])

      self._submeshes[str_matname] = mat

      elem_map_start += num_el_in_blk
      i = i + 1

    self._vertices = ncfile.variables['coord']
    self._num_vertices = ncfile.dimensions['num_nodes']

  # end __init__



  def submesh(self, name):

    r'''
    Return the submesh associated with a name.
    '''

    return self._submeshes[name]

  # end submesh()



  def submesh_names(self):

    r'''
    Return a list of available submeshes (in the correct order).
    '''

    return self._submesh_names
  
  # end available()



  def vertices(self):

    r'''
    Return the list of vertices.
    '''

    return self._vertices

  # end vertices()



  def num_vertices(self):

    r'''
    Return the number of vertices.
    '''

    return self._num_vertices

  # end num_vertices()



  def log(self):

    r'''
    Return the logger for this object.
    '''

    return self._log

  # end log()

# end class
