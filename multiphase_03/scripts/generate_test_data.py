import sys
import numpy
import logging

from scipy.io import netcdf

from exodusII import *

def main():

    file_name = "../multiphase.e"
    ncfile = netcdf.netcdf_file(file_name, 'r')

    submesh_manager = SubmeshManager(ncfile)

    vertices = submesh_manager.vertices()

    submesh_names = []
    submesh_vertices = []

    for submesh_name in submesh_manager.submesh_names():

        submesh = submesh_manager.submesh(submesh_name)

        connectivity = submesh.connect()

        for i in range(submesh.nelems()):

            e = (
                connectivity[i][0]-1, 
                connectivity[i][1]-1,
                connectivity[i][2]-1,
                connectivity[i][3]-1
            )

            for n in range(4):

                vx = vertices[0][e[n]]
                vy = vertices[1][e[n]]
                vz = vertices[2][e[n]]

                index = vertex_is_in((vx,vy,vz), submesh_vertices)
                if index == -1:
                    # Vertex is not in submesh_vertices

                    submesh_vertices.append( (vx,vy,vz) )
                    submesh_names.append({submesh_name:1})

                else :
                    # Vertex IS in submesh_vertices
                    submesh_names[index][submesh_name]=1

    for i in range(len(submesh_vertices)):

        submeshes = "\t".join(submesh_names[i].keys())
        x = submesh_vertices[i][0]
        y = submesh_vertices[i][1]
        z = submesh_vertices[i][2]

        print("{x:20.15}\t{y:20.15}\t{z:20.15}\t{materials}".format(x=x, y=y, z=z, materials=submeshes))



def vertex_is_in(v, array):

    i = 0
    for u in array:
        if ( (u[0]-v[0])**2 + (u[1]-v[1])**2 + (u[2]-v[2])**2 ) < 1E-5:
            return i
        i += 1

    return -1



if __name__ == '__main__':

    main()
