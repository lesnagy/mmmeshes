#!/usr/bin/env python

'''
Generate a script for a cube.
'''

import sys, getopt
from math import *

class ScriptGenerationException(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)



def genCube(totalVolume, elementSize, shellWidth, shellCorrect):
    r'''
    Generate a cubit script for a homogenous cube.
    
    Input:
        totalVolume  - the total volume of the material (micrometer^3)
        elementSize  - the 'size' used for an element (in micrometer)
        shellWidth   - the 'width' used in when applying shellCorrect
        shellCorrect - if `width' of the mapped region doesn't have enough
                       elements, then increse the outer width so that 
                       more elements can fit in. This is better than 
                       auto adjusting element sizes since we can honour 
                       the user's request for a fixed element size in
                       `elementSize.
    Output:
        String containing a cubit script that will generate a homogenous cube
        consisting of a single material.
    '''

    # Material one volume is the total volume, material two's volume will be 
    # subtracted from this to give a shell.
    mat1_volume = totalVolume                     # Total

    # Element size in nm and microns
    element_micron = elementSize
    element_nm     = elementSize*1000.0

    # The 'length' of materials one and two in nm and microns.
    mat1_length_micron = pow(mat1_volume, 1.0/3.0)
    mat1_length_nm = mat1_length_micron * 1000.0

    # The 'unmapped space' radius (in nm and microns)
    r1_micron = (0.2)*mat1_length_micron + mat1_length_micron
    r1_nm     = (0.2)*mat1_length_nm + mat1_length_nm
    
    # The 'mapped space' radius (in microns)
    r0_micron = (4.0/3.0)*r1_micron
    r0_nm     = (4.0/3.0)*r1_nm

    if shellWidth == None and shellCorrect == False:
        pass # A warning should have already been given.
    elif shellWidth != None and shellCorrect == False:
        # Test that there is a width of at least 6 elements between r1 and r0
        if (r0_micron - r1_micron)/element_micron < shellWidth and shellCorrect == False:
            print "WARNING! I too few elements in mapped space."
    elif shellWidth == None and shellCorrect == True:
        pass # Should never get here as program should have already exited.
    elif shellWidth != None and shellCorrect == True:
        # Test that mapped space is 'wide' enough, if not increase infinity 
        # radius.
        if (r0_micron - r1_micron)/element_micron < shellWidth:
            r0_micron = r1_micron + element_micron * shellWidth
            r0_nm     = r0_micron * 1000.0
            print "Infinity radius correction was applied."

    # Read the template from the template file.
    template = ""
    with open("Cube.template", "r") as template_file:
        template = template_file.read()
    
    # Fill in the template `blanks` and return the script as a string.
    return template.format(
            mat1_length_nm = mat1_length_nm,
            mat1_length_micron = mat1_length_micron,
            element_nm = element_nm,
            element_micron = element_micron,
            r0_nm = r0_nm,
            r0_micron = r0_micron,
            r1_nm = r1_nm,
            r1_micron = r1_micron
    )



def helpMessage():
    r''' 
    Return a string containing a help message.
    '''

    message = '''
        Description:
            This script generates cubit scripts for a homogeous cube encased
            in a sphere. The sphere is then encased in a subsequent sphere 
            which represents mapped space; it is in this region that the 
            spherical transform is applied.
        Usage: 
            ./gen-script.py [options]
        options:
            -h [optional]
                Print this help message and exit.
            -o --ofile  [required] TYPE: String  UNIT: N/A
                The name of the output file.

            -v --volume [required: see -l] TYPE: Float UNIT: micrometers
                The volume of the cube, if -l is set this is not required however
                setting both -v and -l results in a warning and -v takes precedence.

            -l --length [required: see -v] TYPE: Float UNIT: micrometers
                The length of the cube, if -v is set this is not required however
                setting both -v and -l results in a warning and -v takes precedence.

            -e --element-size [required] TYPE: Float UNIT: micrometers
                The 'size' of an element.

            --shell-width [optional: see --shell-correct ] TYPE: int UNIT: N/A
                The minimum 'width' of the mapped region of space in elements, this
                is only required if --shell-correct is enabled, however if 
                --shell-correct is disabled (which it is by default) 
                then --shell-width should still be set (a warning is produced if 
                it is not) and this case an additional warning informs the user 
                if  the width of the mapped region is not wide enough. If 
                --shell-correct has been set, then this is a required parameter. 
                NOTE: if --shell-width and --shell-correct are *BOTH* unset then 
                this can result in an unmapped region that is too thin.

            --shell-correct [optional: see --shell-width]
                Enable element correction (by default it is disabled). Element 
                correction will calculate the approximate 'width' of the mapped
                space region, if this 'width' is smaller than --shell-width 
                the 'infinity radius' will be increased to fit as many elements
                as required. If element --shell-correct is disabled (which
                is the default behaviour) but --shell-width is set then
                a warning is produces informing the user that the width of 
                the mapped retion is not wide enough. NOTE: if --shell-width
                and --shell-correct are *BOTH* unset then this can 
                result in an unmapped region that is too thin.
    '''

    return message



def main(argv):

    outputfile   = None
    length       = None
    volume       = None
    elementSize  = None
    shellWidth   = None
    shellCorrect = False

    # Set command line options.
    try:
        opts, args = getopt.getopt(
                argv, 
                "ho:l:v:e:",
                [
                    "ofile=",
                    "length=",
                    "volume=",
                    "element-size=",
                    "shell-width=",
                    "shell-correct"
                ]
        )
    except getopt.GetoptError:
        print helpMessage()
        sys.exit(2)

    # Process command line options and values.
    for opt, arg in opts:
        if opt == "-h":
            print helpMessage()
            sys.exit()
        elif opt in ("-o", "--ofile"):
            outputfile = arg
        elif opt in ("-l", "--length"):
            length = float(arg)
        elif opt in ("-v", "--volume"):
            volume = float(arg)
        elif opt in ("-e", "--element-size"):
            elementSize = float(arg)
        elif opt in ("--shell-width"):
            shellWidth = int(arg)
        elif opt in ("--shell-correct"):
            shellCorrect = True

    # Verify command line options and values.

    if outputfile == None:
        print "ERROR: Output file name required."
        print helpMessage()
        sys.exit(2)

    if elementSize == None:
        print "ERROR: Element size is required."
        print helpMessage()
        sys.exit(2)

    if length == None and volume == None:
        print "ERROR: At least one of length or volume required."
        print helpMessage()
        sys.exit(2)
    elif length != None and volume == None:
        volume = length**3
    elif length == None and volume != None:
        pass 
    elif length != None and volume != None:
        print "WARNING: length and volume given, using volume value."

    if shellWidth == None and shellCorrect == False:
        print "WARNING: --shell-width is not set, consider setting this!"
    elif shellWidth != None and shellCorrect == False:
        pass
    elif shellWidth == None and shellCorrect == True:
        print "ERROR: setting --shell-correct requires that --shell-width be given a value."
        print helpMessage()
        sys.exit(2)
    elif shellWidth != None and shellCorrect == True:
        pass

    script = genCube(volume, elementSize, shellWidth, shellCorrect)
    with open(outputfile, "w") as fout:
        fout.write(script)



if __name__ == "__main__":
    main(sys.argv[1:])
    
    
