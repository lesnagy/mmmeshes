#!/usr/bin/env python
r'''
Small utility to read a file in sparse data format and display.
'''

if __name__ == '__main__':

    fin = open('grad.dat', 'r')

    line = fin.readline()
    
    NMAX = int(line.split()[0])
    NNZ  = int(line.split()[1])

    row_ptr = []
    col_idx = []
    val     = []

    for i in range(NNZ):

        line = fin.readline().strip()

        if i < NMAX+1:
            
            row_ptr.append(int(line.split()[0])-1)
            col_idx.append(int(line.split()[1])-1)
            val.append(float(line.split()[2]))

        else:

            col_idx.append(int(line.split()[0])-1)
            val.append(float(line.split()[1]))

    dense = []
    for i in range(NMAX):
        dense.append([])
        for j in range(NMAX):
            dense[i].append(0.0)

    l = 0
    for i in range(NMAX):
        for j in range(row_ptr[i], row_ptr[i+1]):
            dense[i][col_idx[j]] = val[l]
            l = l + 1

    for i in range(NMAX):
        for j in range(NMAX):
            print ("%10.5f" % dense[i][j]),
        print ""

