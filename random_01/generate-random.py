#!/usr/bin/env python

import sys
import getopt
import os

from dolfin import *

import micromag.cpp

import logging
logging.basicConfig()
LOG_LEVEL = logging.DEBUG

class ProgramOptions:
    r'''
    Class to hold program options.
    '''

    def __init__(self, argv):
        r'''
        Create an object that will parse and verify command line arguments.
        '''

        self._logger = logging.getLogger('ProgramOptions')
        self._logger.setLevel(LOG_LEVEL)

        self.debug("Initialise `mesh_file`, `mesh_function_file`, `indices`")

        self._mesh_file          = None
        self._mesh_function_file = None
        self._indices            = []
        self._out_file           = None
        self._seed               = None

        self.debug("Parse command line options")

        try:
            opts, args = getopt.getopt (

                    argv, 
                    "h", 
                    [
                        "help", 
                        "mesh=", 
                        "meshfun=", 
                        "indices=", 
                        "output=", 
                        "seed="
                    ]
                    
            )
        except getopt.GetoptError:
            print self.help_message()
            sys.exit()

        # Parse command line arguments

        for opt, arg in opts:

            if opt in ['-h', '--help']:
                self.debug("Found help switch")
                print self.help_message()
                sys.exit()

            elif opt == '--mesh':
                self.debug("Found mesh file switch")
                if os.path.isfile(arg):
                    self.debug("Checking to see if file exists")
                    self._mesh_file = arg
                    self.debug("OK! (file '%s')" % self._mesh_file)
                else:
                    print "Cannot find mesh file '%s'" % arg
                    sys.exit()

            elif opt == '--meshfun':
                self.debug("Found mesh function file switch")
                if os.path.isfile(arg):
                    self.debug("Checking to see if file exists")
                    self._mesh_function_file = arg
                    self.debug("OK! (file '%s')" % self._mesh_function_file)
                else:
                    print "Cannot find mesh function file '%s'" % arg
                    sys.exit()

            elif opt == '--indices':
                self.debug("Found list of indices")
                idxs = arg.split(',')
                for idx in idxs:
                    try:
                        self._indices.append(int(idx))
                    except ValueError:
                        print "Value '%s' in index list is not an integer"
                        sys.exit()
                self.debug("OK! (indices: %s)" % self._indices)

            elif opt == '--output':
                self._out_file = arg
                self.debug("Found output file (file '%s'" % self._out_file)

            elif opt == '--seed':
                self.debug("Found a seed")
                try:
                    self._seed = int(arg)
                except ValueError:
                    print "Value '%s' for seed is not an integer"
                    sys.exit()
                self.debug("OK! (seed: %d)" % self._seed)
                


        # Check that input parameter have been defined

        if self._mesh_file == None:
            print "No mesh file supplied, use -h or --help for usage."
            sys.exit()

        if self._mesh_function_file == None:
            print "No mesh function file supplied, use -h or --help for usage."
            sys.exit()

        if self._out_file == None:
            print "No output file supplied, use -h or --help for usage."
            sys.exit()



    def mesh_file(self):

        r'''
        Retrieve the mesh file name.
        '''

        return self._mesh_file



    def mesh_function_file(self):

        r'''
        Retrieve the mesh function file.
        '''

        return self._mesh_function_file



    def indices(self):

        r'''
        Retrieve the list of submesh indices.
        '''

        return self._indices



    def out_file(self):

        r'''
        Retrive the name of the output file.
        '''

        return self._out_file



    def seed(self):

        r'''
        Retrieve the seed value.
        '''

        return self._seed



    def debug(self, message):

        r'''
        Send a debug message to the logger.
        '''

        self._logger.debug(message)



    def help_message(self):

        r'''
        Construct a help message text for the user.
        '''

        return "This script will generate a random field for a given mesh.\n" \
               "Options include:\n"                                           \
               "--mesh=<mesh file> [required]\n"                              \
               "    name of the input mesh file in dolfin xml format.\n"      \
               "\n"                                                           \
               "--meshfun=<mesh function file> [required]\n"                  \
               "    name of the input mesh function file\n"                   \
               "\n"                                                           \
               "--indices=<comma separated list of indices> [optional]"       \
               "    the list of submesh indices for which we would like to \n"\
               "    generate a random field\n"                                \
               "\n"                                                           \
               "--output=<output file name> [required]\n"                     \
               "    the name of the output file that the random field will\n" \
               "    be written to.\n"                                         \
               "\n"                                                           \
               "-h or --help [optional]\n"                                    \
               "    will print this help message\n"                           



    def __str__(self):

        r'''
        Return a string representation of this object.
        '''

        return "         Mesh file: '%s'\n"                                   \
               "Mesh function file: '%s'\n"                                   \
               "   Submesh indices:  %s \n"                                   \
               "              Seed:  %s \n"                                   \
               "       Output file: '%s'\n" %                                 \
               (
                    self.mesh_file(), 
                    self.mesh_function_file(), 
                    self.indices(), 
                    self.seed(), 
                    self.out_file()
               )
        


if __name__ == '__main__':

    logger = logging.getLogger('main')
    logger.setLevel(LOG_LEVEL)

    popts = ProgramOptions(sys.argv[1:])
    print popts

    logger.debug("Creating mesh and mesh function data structures")

    mesh = Mesh(popts.mesh_file())
    meshfn = MeshFunction('size_t', mesh, popts.mesh_function_file())

    logger.debug("Creating scalar/vector function spaces.")

    V = FunctionSpace(mesh, 'CG', 1)
    VV = VectorFunctionSpace(mesh, 'CG', 1)

    logger.debug("Creating dof_manager")

    dof_manager = None
    if popts.indices() == []:

        logger.debug("Indices list empty "                                    \
                     "creating dof_manager for whole mesh")

        dof_manager = micromag.cpp.dof_manager_t(mesh, V, VV)

    else:

        logger.debug("Inidices list not empty "                               \
                     "Creating dof_manager for submeshes")

        materials = micromag.cpp.material_list_t()

        for i in popts.indices():

            materials.append (
                micromag.cpp.material_t (
                    i, "DummyName%i"%i, 1.0,
                    micromag.cpp.axis_t(1.0, 0.0, 0.0),
                    micromag.cpp.axis_t(0.0, 1.0, 0.0),
                    micromag.cpp.axis_t(0.0, 0.0, 1.0),
                    1.0, 1.0
                )
            )
        
        dof_manager = micromag.cpp.dof_manager_t (
            mesh, 
            V, 
            VV, 
            meshfn, 
            materials
        )

    assert(dof_manager != None)

    logger.debug("Creating random operation")

    random = None
    if (popts.seed() == None):
        random = micromag.cpp.random_t ( 
            dof_manager,
            micromag.cpp.ZERO
        )
    else:
        random = micromag.cpp.random_t (
            dof_manager, 
            micromag.cpp.ZERO,
            popts.seed()
        )

    assert (random != None)

    logger.debug("Creating vectors")

    v = Function(VV)

    random.perform(v)

    fout = File(popts.out_file())
    fout << v
 
