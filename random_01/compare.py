#!/usr/bin/env python

import os, sys, math
import vtk

import CMDLine

#
# Program main function.
#
def main():
  
  cmdline = CMDLine.CMDLine()
  
  # Attempt to read vtk files 1 & 2
  reader1 = vtk.vtkXMLUnstructuredGridReader()
  reader2 = vtk.vtkXMLUnstructuredGridReader()
  
  reader1.SetFileName(cmdline.filename1)
  reader2.SetFileName(cmdline.filename2)
  reader1.Update()
  reader2.Update()
  ugrid1 = reader1.GetOutput()
  ugrid2 = reader2.GetOutput()
  
  # Number of cells (elements) and points (nodes)
  ncells1  = ugrid1.GetNumberOfCells()
  npoints1 = ugrid1.GetNumberOfPoints()
  ncells2  = ugrid2.GetNumberOfCells()
  npoints2 = ugrid2.GetNumberOfPoints()
  
  if ncells1 != ncells2:
    print "Number of elements in %s and %s do not match!" %\
      (cmdline.filename1, cmdline.filename2)
    sys.exit(1)
    
  if npoints1 != npoints2:
    print "Number of nodes in %s and %s do not match!" %\
      (cmdline.filename1, cmdline.filename2)
    sys.exit(1)
      
  ncells  = ncells1
  npoints = npoints1
  
  # The magnetisation vectors
  m1 = ugrid1.GetPointData().GetVectors()
  m2 = ugrid2.GetPointData().GetVectors()
  if m1.GetNumberOfTuples() != npoints:
    print "WARNING: number of vectors in %s does not match number of points" %\
      (cmdline.filename1)
  if m2.GetNumberOfTuples() != npoints:
    print "WARNING: number of vectors in %s does not match number of points" %\
      (cmdline.filename2)
  
  # The node coordinates (should be the same)
  coords1 = ugrid1.GetPoints()
  coords2 = ugrid2.GetPoints()
  
  materialPoints = vtk.vtkPoints()
  diffArray = vtk.vtkFloatArray()
  diffArray.SetName("differences")
  diffArray.SetNumberOfComponents(1)
  diffArray.SetNumberOfValues(npoints)
  maxDiff = -1.0
  minDiff =  1E12
  for i in range(npoints):
    m1x = m1.GetTuple(i)[0]
    m1y = m1.GetTuple(i)[1]
    m1z = m1.GetTuple(i)[2]
    m2x = m2.GetTuple(i)[0]
    m2y = m2.GetTuple(i)[1]
    m2z = m2.GetTuple(i)[2]
    
    len1 = math.sqrt(m1x**2 + m1y**2 + m1z**2)
    len2 = math.sqrt(m2x**2 + m2y**2 + m2z**2)
    
    if (len1 > 0.0) and (len2 > 0.0):
      x1 = coords1.GetPoint(i)[0]
      y1 = coords1.GetPoint(i)[1]
      z1 = coords1.GetPoint(i)[2]
      x2 = coords2.GetPoint(i)[0]
      y2 = coords2.GetPoint(i)[1]
      z2 = coords2.GetPoint(i)[2]
      
      diffAtPoint = math.sqrt( (m1x - m2x)**2 + (m1y - m2y)**2 + (m1z - m1z)**2 )
      
      if diffAtPoint > maxDiff:
        maxDiff = diffAtPoint
        
      if diffAtPoint < minDiff:
        minDiff = diffAtPoint
        
      diffArray.SetValue(i, diffAtPoint)
      
      if x1 != x2:
        print "X coordinate in one mesh didn't match x coordinate in another"
      if y1 != y2:
        print "Y coordinate in one mesh didn't match y coordinate in another"
      if z1 != z2:
        print "Z coordinate in one mesh didn't match z coordinate in another"
        
      materialPoints.InsertNextPoint(x1, y1, z1)
  
  print "Maximum difference: %f" % maxDiff
  print "Minimum difference: %f" % minDiff

if __name__ == '__main__':
  main()
