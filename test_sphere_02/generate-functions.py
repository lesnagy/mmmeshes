#!/usr/bin/env python

from dolfin import *

mesh = Mesh('mesh.xml')
meshfun = MeshFunction('size_t', mesh, 'mesh_meshfn.xml')
VV = VectorFunctionSpace(mesh, 'CG', 1)

u = interpolate(Expression((
      '(2*x[0]*x[1])/(sqrt(1 + 4*x[0]*x[0]*(x[1]*x[1] + x[2]*x[2])))',
      '(2*x[0]*x[2])/(sqrt(1 + 4*x[0]*x[0]*(x[1]*x[1] + x[2]*x[2])))',
      '            1/(sqrt(1 + 4*x[0]*x[0]*(x[1]*x[1] + x[2]*x[2])))'
    )), VV)

v = interpolate(Expression((
      '-x[1]/sqrt(0.25 +    x[0]*x[0] +   x[1]*x[1])',
      ' x[0]/sqrt(0.25 +    x[0]*x[0] +   x[1]*x[1])',
      '    1/sqrt(   1 +  4*x[0]*x[0] + 4*x[1]*x[1])'
    )), VV)

fout = File('u.pvd')
fout << u
fout = File('u.xml')
fout << u

fout = File('v.pvd')
fout << v
fout = File('v.xml')
fout << v
